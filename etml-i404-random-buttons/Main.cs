﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace etml_i404_random_buttons
{
    public partial class frmMain : Form
    {
        // Texte du compteur
        private const string COUNTER_TEXT = "Nombre de boutons: ";

        // Nombre de boutons
        private int _btnCount;

        // Coins du container
        private Point topLeft;
        private Point topRight;
        private Point bottomLeft;
        private Point bottomRight;

        // Générateur de nombres aléatoires
        private Random rdm = new Random();

        public frmMain()
        {
            InitializeComponent();
            _btnCount = 1;
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            ++_btnCount;
            int x = rdm.Next(panContainer.Width);
            int y = rdm.Next(panContainer.Height);

            // Création du bouton
            Button newBtn = new Button();
            newBtn.Location = new Point(x, y);
            newBtn.Text = $"Bouton {_btnCount}";
            newBtn.Click += new System.EventHandler(btn1_Click);

            // Ajout au container et incrémentation
            panContainer.Controls.Add(newBtn);
            lblNbOfButtons.Text = COUNTER_TEXT + _btnCount.ToString();
        }
    }
}
