﻿namespace etml_i404_random_buttons
{
    partial class frmMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNbOfButtons = new System.Windows.Forms.Label();
            this.btn1 = new System.Windows.Forms.Button();
            this.panContainer = new System.Windows.Forms.Panel();
            this.panContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblNbOfButtons
            // 
            this.lblNbOfButtons.AutoSize = true;
            this.lblNbOfButtons.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNbOfButtons.Location = new System.Drawing.Point(12, 349);
            this.lblNbOfButtons.Name = "lblNbOfButtons";
            this.lblNbOfButtons.Size = new System.Drawing.Size(166, 20);
            this.lblNbOfButtons.TabIndex = 2;
            this.lblNbOfButtons.Text = "Nombre de boutons: 1";
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(4, 3);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(75, 23);
            this.btn1.TabIndex = 0;
            this.btn1.Text = "Bouton 1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // panContainer
            // 
            this.panContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panContainer.Controls.Add(this.btn1);
            this.panContainer.Location = new System.Drawing.Point(12, 13);
            this.panContainer.Name = "panContainer";
            this.panContainer.Size = new System.Drawing.Size(560, 330);
            this.panContainer.TabIndex = 3;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 381);
            this.Controls.Add(this.panContainer);
            this.Controls.Add(this.lblNbOfButtons);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Boutons à tous vents";
            this.panContainer.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblNbOfButtons;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Panel panContainer;
    }
}

